package com.telstra.testdemo;





import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.telstra.springbootdemo.model.Employee;

public class EmployeeTest {

	@Test
	public void getSalaryTest() {
		Employee employee = new Employee(1001,"Ram","manager",50000);
		assertEquals(50000,employee.getSalary());
	}
	
		
}